class User {
  final String uid;
  final String name;
  final String surname;
  final int age;

  User({this.uid, this.name, this.surname, this.age});
}
