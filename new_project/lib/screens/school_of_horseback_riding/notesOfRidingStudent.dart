import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/school_of_horseback_riding/addNoteOfRidingStudent.dart';
import 'package:new_project/shared/DynamicallyCheckbox.dart';
import 'package:new_project/shared/methods.dart';

class NotesRidingStudent extends StatefulWidget {
  final String name;
  final String surname;
  final String horse;
  final String ridingStudentId;
  List<String> doneTasks;

  NotesRidingStudent(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      List<String> doneTasks});

  @override
  _NotesRidingStudentState createState() => _NotesRidingStudentState(
      name: this.name,
      surname: this.surname,
      horse: this.horse,
      ridingStudentId: this.ridingStudentId,
      doneTasks: this.doneTasks);
}

class _NotesRidingStudentState extends State<NotesRidingStudent> {
  final String name;
  final String surname;
  String horse;
  final String ridingStudentId;
  List<String> doneTasks;

  TextEditingController horseInputController;
  TextEditingController durationInputController;
  TextEditingController descriptionInputController;

  _NotesRidingStudentState(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      this.doneTasks});
  var finaldate;

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finaldate = order;
    });
  }

  @override
  void initState() {
    this.doneTasks = new List();
    horseInputController = new TextEditingController(text: this.horse);
    descriptionInputController = new TextEditingController();
    durationInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog(tasks) async {
      Map<String, bool> taskMap = tasks;

      await showDialog<String>(
          context: context,
          child: AlertDialog(
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Konj',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown))),
                      controller: horseInputController,
                    ),
                    /*FlatButton(
                      child: Text('Odaberi obavljene zadatke'),
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) =>
                                AddNoteOfRidingStudent(
                                  name: this.name,
                                  surname: this.surname,
                                  ridingStudentId: this.ridingStudentId,
                                  tasks: taskMap,
                                  horse: this.horse,
                                  doneTasks: this.doneTasks,
                                )));
                      },
                    ),*/
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: 'Trajanje',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                            ),
                            controller: durationInputController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: 'Opis',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.brown)),
                            ),
                            controller: descriptionInputController,
                          ),
                        ),
                      ],
                    ),
                    new RaisedButton(
                      onPressed: callDatePicker,
                      color: Colors.brown[400],
                      child: new Text(
                        'Odaberi datum',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  durationInputController.clear();
                  descriptionInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text(
                  'Dodaj',
                ),
                onPressed: () {
                  if (horseInputController.text.isNotEmpty &&
                      durationInputController.text.isNotEmpty &&
                      finaldate != null) {
                    FirebaseFirestore.instance
                        .collection('biljeskePolazniciSkole')
                        .add({
                          "student": this.ridingStudentId,
                          "datum": finaldate,
                          "trajanje": int.parse(durationInputController.text),
                          "opis": descriptionInputController.text.isEmpty
                              ? ""
                              : descriptionInputController.text,
                          "konj": horseInputController.text,
                          "zadatci": this.doneTasks
                        })
                        .then((result) => {
                              Navigator.pop(context),
                            })
                        .catchError((err) => print(err));
                  }
                },
              ),
            ],
          ));
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Bilješke - ' + this.name + ' ' + this.surname,
          ),
          backgroundColor: Colors.brown[400],
        ),
        body: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('biljeskePolazniciSkole')
              .where('student', isEqualTo: this.ridingStudentId)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: Text('Trenutak!'),
              );
            }

            return ListView(
              children: snapshot.data.docs.map((document) {
                return Card(
                  child: ListTile(
                    title: Text(timestampToString(document['datum'])),
                    onTap: () async {
                      await showDialog(
                          context: context,
                          child: new AlertDialog(
                            content: Container(
                              child: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    new Text('Konj: ' + document['konj']),
                                    new Text('Trajanje: ' +
                                        document['trajanje'].toString() +
                                        ' minuta'),
                                    new Text('Opis: ' + document['opis']),
                                  ],
                                ),
                              ),
                            ),
                            actions: <Widget>[
                              FlatButton(
                                textColor: Colors.white,
                                color: Colors.brown[400],
                                child: Text(
                                  'Izađi',
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ));
                    },
                    onLongPress: () async {
                      await showDialog(
                          context: context,
                          child: new AlertDialog(
                            content: new Text(
                              'Želite li obrisati bilješku?',
                              style: TextStyle(color: Colors.brown),
                            ),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(
                                  'Odustani',
                                  style: TextStyle(color: Colors.brown),
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                              ),
                              FlatButton(
                                textColor: Colors.white,
                                color: Colors.brown[400],
                                child: Text('Obriši'),
                                onPressed: () {
                                  FirebaseFirestore.instance
                                      .collection('biljeskePolazniciSkole')
                                      .doc(document.id)
                                      .delete();
                                  Navigator.pop(context);
                                },
                              )
                            ],
                          ));
                    },
                  ),
                );
              }).toList(),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.brown[400],
          onPressed: () async {
            Map<String, bool> tasks = new Map();
            await FirebaseFirestore.instance
                .collection('zadatciSkolaJahanja')
                .get()
                .then((querySnapshot) {
              querySnapshot.docs.forEach((element) {
                tasks[element['opis']] = false;
                print('hej' + tasks[element['opis']].toString());
              });
              _showDialog(tasks);
            });
          },
          tooltip: 'Dodaj',
          child: Icon(Icons.add),
        ));
  }
}
