import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/school_of_horseback_riding/notesOfRidingStudent.dart';
import 'package:new_project/screens/school_of_horseback_riding/paymentOfRidingStudent.dart';
import 'package:new_project/services/horseDatabase.dart';

class SpecificRidingStudent extends StatefulWidget {
  final String name;
  final String surname;
  final String ridingStudentId;
  final String horse;

  SpecificRidingStudent(
      {@required this.name,
      @required this.surname,
      @required this.ridingStudentId,
      @required this.horse});

  @override
  _SpecificRidingStudentState createState() => _SpecificRidingStudentState(
      name: this.name,
      surname: this.surname,
      ridingStudentId: this.ridingStudentId,
      horse: this.horse);
}

class _SpecificRidingStudentState extends State<SpecificRidingStudent> {
  final String name;
  final String surname;
  String horse;
  final String ridingStudentId;

  TextEditingController ageInputController;
  TextEditingController heightInputController;
  TextEditingController weightInputController;

  _SpecificRidingStudentState(
      {@required this.name,
      @required this.surname,
      @required this.ridingStudentId,
      @required this.horse});

  @override
  void initState() {
    ageInputController = new TextEditingController();
    heightInputController = new TextEditingController();
    weightInputController = new TextEditingController();
    super.initState();
  }

  void setHorse(String value) {
    setState(() {
      this.horse = value;
      print(value);
    });
  }

  @override
  Widget build(BuildContext context) {
    _showDialog(horsesNames) async {
      List<String> horseNames = horsesNames;

      await showDialog<String>(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                          labelText: 'Dob*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                        ),
                        controller: ageInputController,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          labelText: 'Visina*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                        ),
                        controller: heightInputController,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                          labelText: 'Težina*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                        ),
                        controller: weightInputController,
                      ),
                    ),
                    Row(children: [
                      Text(
                        'Odaberi konja: ',
                        style: TextStyle(color: Colors.brown),
                      ),
                      Expanded(
                        child: DropdownButton<String>(
                          value: this.horse,
                          isDense: true,
                          onChanged: (String newValue) {
                            setHorse(newValue);
                          },
                          items: horseNames.map((value) {
                            return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(
                                  value,
                                  style: TextStyle(
                                    color: Colors.brown,
                                  ),
                                ));
                          }).toList(),
                        ),
                      ),
                    ]),
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text(
                  'Spremi promjene',
                ),
                onPressed: () {
                  if (ageInputController.text.isNotEmpty &&
                      weightInputController.text.isNotEmpty &&
                      heightInputController.text.isNotEmpty) {
                    FirebaseFirestore.instance
                        .collection('polazniciSkole')
                        .doc(this.ridingStudentId)
                        .set({
                          "dob": int.parse(ageInputController.text),
                          "visina": int.parse(heightInputController.text),
                          "težina": int.parse(weightInputController.text),
                          "ime": this.name,
                          "prezime": this.surname,
                          "konj": this.horse
                        })
                        .then((result) => {
                              Navigator.pop(context),
                            })
                        .catchError((err) => print(err));
                  }
                },
              ),
            ],
          ));
    }

    return Scaffold(
        appBar: AppBar(
          title: Text(this.name + " " + this.surname),
          backgroundColor: Colors.brown[400],
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              ListTile(
                title: Text("Plaćanja"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => PaymentRidingStudent(
                          ridingStudentId: this.ridingStudentId,
                          name: this.name,
                          surname: this.surname)));
                },
              ),
              ListTile(
                title: Text("Bilješke"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => NotesRidingStudent(
                          name: this.name,
                          surname: this.surname,
                          horse: this.horse,
                          ridingStudentId: this.ridingStudentId)));
                },
              )
            ],
          ),
        ),
        body: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection('polazniciSkole')
              .doc(this.ridingStudentId)
              .snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return new Text('Trenutak!');
            }
            var student = snapshot.data;
            this.ageInputController =
                new TextEditingController(text: student['dob'].toString());
            this.heightInputController =
                new TextEditingController(text: student['visina'].toString());
            this.weightInputController =
                new TextEditingController(text: student['težina'].toString());
            return Container(
              padding: EdgeInsets.all(20.0),
              child: Table(
                border: TableBorder.all(color: Colors.white),
                children: [
                  TableRow(children: [
                    Text('Dob:'),
                    Text(
                      student['dob'].toString() + " god",
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]),
                  TableRow(children: [
                    Text('Visina:'),
                    Text(
                      student['visina'].toString() + " cm",
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]),
                  TableRow(children: [
                    Text('Težina:'),
                    Text(
                      student['težina'].toString() + " kg",
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]),
                  TableRow(children: [
                    Text('Konj:'),
                    Text(student['konj'],
                        style: Theme.of(context).textTheme.bodyText1)
                  ])
                ],
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.brown[400],
          onPressed: () async {
            List<String> horseNames = new List();
            await FirebaseFirestore.instance
                .collection("konji")
                .get()
                .then((querySnapshot) {
              querySnapshot.docs.forEach((element) {
                horseNames.add(element['ime']);
              });
              _showDialog(horseNames);
            });
          },
          tooltip: 'Izmijeni',
          child: Icon(Icons.edit),
        ));
  }
}
