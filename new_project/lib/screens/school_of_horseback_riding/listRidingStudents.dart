import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/school_of_horseback_riding/addNewRidingStudent.dart';
import 'package:new_project/screens/school_of_horseback_riding/specificRidingStudent.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/addNewTUser.dart';

class ListRidingStudents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Polaznici škole jahanja'),
        backgroundColor: Colors.brown[400],
      ),
      body: new StreamBuilder(
        stream:
            FirebaseFirestore.instance.collection('polazniciSkole').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
              children: snapshot.data.docs.map((document) {
            return Card(
              child: ListTile(
                title: Text(document['ime'] + ' ' + document['prezime']),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SpecificRidingStudent(
                              name: document['ime'],
                              surname: document['prezime'],
                              ridingStudentId: document.id,
                              horse: document['konj'])));
                },
                onLongPress: () async {
                  await showDialog(
                      context: context,
                      child: new AlertDialog(
                        content: new Text(
                          'Želite li obrisati polaznika?',
                          style: TextStyle(color: Colors.brown),
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              'Odustani',
                              style: TextStyle(color: Colors.brown),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            color: Colors.brown[400],
                            textColor: Colors.white,
                            child: Text(
                              'Obriši',
                            ),
                            onPressed: () {
                              FirebaseFirestore.instance
                                  .collection('polazniciSkole')
                                  .doc(document.id)
                                  .delete();
                              Navigator.pop(context);
                            },
                          )
                        ],
                      ));
                },
              ),
            );
          }).toList());
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () async {
          List<String> horseNames = new List();
          await FirebaseFirestore.instance
              .collection("konji")
              .get()
              .then((querySnapshot) {
            querySnapshot.docs.forEach((element) {
              horseNames.add(element['ime']);
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddRidingStudent(horseNames)));
          });
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
