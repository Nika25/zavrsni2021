import 'package:flutter/material.dart';
import 'package:new_project/shared/DynamicallyCheckbox.dart';

class AddNoteOfRidingStudent extends StatefulWidget {
  final String name;
  final String surname;
  final String horse;
  final String ridingStudentId;
  final Map<String, bool> tasks;
  List<String> doneTasks;

  AddNoteOfRidingStudent(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      @required this.tasks,
      this.doneTasks});
  @override
  _AddNoteOfRidingStudentState createState() => _AddNoteOfRidingStudentState(
      name: this.name,
      surname: this.surname,
      horse: this.horse,
      ridingStudentId: this.ridingStudentId,
      tasks: this.tasks,
      doneTasks: this.doneTasks);
}

class _AddNoteOfRidingStudentState extends State<AddNoteOfRidingStudent> {
  final String name;
  final String surname;
  String horse;
  final String ridingStudentId;
  List<String> doneTasks;
  Map<String, bool> tasks;

  _AddNoteOfRidingStudentState(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      @required this.tasks,
      this.doneTasks});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj bilješku'),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
          margin: EdgeInsets.all(10.0),
          child: DynamicallyCheckbox(
              name: this.name,
              surname: this.surname,
              horse: this.horse,
              ridingStudentId: this.ridingStudentId,
              list: this.tasks,
              doneTasks: this.doneTasks)),
    );
  }
}
