import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/shared/methods.dart';

class PaymentRidingStudent extends StatefulWidget {
  final String ridingStudentId;
  final String name;
  final String surname;
  PaymentRidingStudent(
      {@required this.ridingStudentId,
      @required this.name,
      @required this.surname});

  @override
  _PaymentRidingStudentState createState() => _PaymentRidingStudentState(
      ridingStudentId: this.ridingStudentId,
      name: this.name,
      surname: this.surname);
}

class _PaymentRidingStudentState extends State<PaymentRidingStudent> {
  final String ridingStudentId;
  final String name;
  final String surname;

  var finalDate;
  TextEditingController paymentCostInputController;

  _PaymentRidingStudentState(
      {@required this.ridingStudentId,
      @required this.name,
      @required this.surname});

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finalDate = order;
    });
  }

  @override
  void initState() {
    paymentCostInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() async {
      await showDialog<String>(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            decoration: InputDecoration(
                              labelText: 'Iznos u kunama*',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                            ),
                            controller: paymentCostInputController,
                          ),
                        ),
                      ],
                    ),
                    new RaisedButton(
                      onPressed: callDatePicker,
                      color: Colors.brown[400],
                      child: new Text(
                        'Odaberi datum',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  paymentCostInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text(
                  'Dodaj',
                ),
                onPressed: () {
                  if (paymentCostInputController.text.isNotEmpty &&
                      finalDate != null) {
                    FirebaseFirestore.instance
                        .collection('placanjaPolazniciSkole')
                        .add({
                          "iznos": int.parse(paymentCostInputController.text),
                          "datum": finalDate,
                          "student": this.ridingStudentId
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              paymentCostInputController.clear(),
                            })
                        .catchError((err) => print(err));
                  }
                },
              )
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Plaćanja - " + this.name + " " + this.surname),
        backgroundColor: Colors.brown[400],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection("placanjaPolazniciSkole")
            .where('student', isEqualTo: this.ridingStudentId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Text(document['iznos'].toString() +
                      ' ' +
                      'kn, ' +
                      timestampToString(document['datum'])),
                  onTap: () async {
                    await showDialog(
                        context: context,
                        child: new AlertDialog(
                          content: new Text("Želite li obrisati sadržaj?",
                              style: TextStyle(color: Colors.brown)),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Odustani',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              textColor: Colors.white,
                              color: Colors.brown[400],
                              child: Text(
                                'Obriši',
                              ),
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('placanjaPolazniciSkole')
                                    .doc(document.id)
                                    .delete();
                                Navigator.pop(context);
                              },
                            ),
                          ],
                        ));
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: _showDialog,
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
