import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddRidingStudent extends StatefulWidget {
  final List<String> horseNames;
  AddRidingStudent(this.horseNames);
  @override
  _AddRidingStudentState createState() =>
      _AddRidingStudentState(this.horseNames);
}

class _AddRidingStudentState extends State<AddRidingStudent> {
  TextEditingController nameInputController;
  TextEditingController surnameInputController;
  TextEditingController ageInputController;
  TextEditingController horseInputController;
  TextEditingController heightInputController;
  TextEditingController weightInputController;
  final List<String> horseNames;
  String horse;

  _AddRidingStudentState(this.horseNames);

  @override
  void initState() {
    nameInputController = new TextEditingController();
    surnameInputController = new TextEditingController();
    ageInputController = new TextEditingController();
    horseInputController = new TextEditingController();
    heightInputController = new TextEditingController();
    weightInputController = new TextEditingController();

    this.horse = horseNames.elementAt(0);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj polaznika škole'),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
          margin: EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                          hintText: 'Ime',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                        ),
                        controller: nameInputController,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                          hintText: 'Prezime',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                        ),
                        controller: surnameInputController,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                          hintText: 'Dob',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                        ),
                        controller: ageInputController,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                          hintText: 'Težina',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                        ),
                        controller: weightInputController,
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                          hintText: 'Visina',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                        ),
                        controller: heightInputController,
                      ),
                    ),
                  ],
                ),
                Row(children: [
                  Text(
                    'Odaberi konja: ',
                    style: TextStyle(color: Colors.brown),
                  ),
                  Expanded(
                    child: DropdownButton<String>(
                      value: this.horse,
                      isDense: true,
                      onChanged: (String newValue) {
                        setState(() {
                          this.horse = newValue;
                          print(this.horse);
                        });
                      },
                      items: horseNames.map((value) {
                        return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(
                              value,
                              style: TextStyle(
                                color: Colors.brown,
                              ),
                            ));
                      }).toList(),
                    ),
                  ),
                ]),
                FlatButton(
                  color: Colors.brown[100],
                  child: Text(
                    'Odustani',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    nameInputController.clear();
                    surnameInputController.clear();
                    ageInputController.clear();
                    weightInputController.clear();
                    heightInputController.clear();
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  color: Colors.brown[400],
                  textColor: Colors.white,
                  child: Text(
                    'Dodaj',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (!nameInputController.text.isNotEmpty ||
                        !surnameInputController.text.isNotEmpty) {
                      showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                                content: Container(
                                  child: Container(
                                    child: Text(
                                        'Potrebno je unijeti ime i prezime!'),
                                  ),
                                ),
                              ));
                    }
                    if (nameInputController.text.isNotEmpty &&
                        surnameInputController.text.isNotEmpty) {
                      FirebaseFirestore.instance
                          .collection('polazniciSkole')
                          .add({
                            "ime": nameInputController.text,
                            "prezime": surnameInputController.text,
                            "dob": ageInputController.text.isEmpty
                                ? int.parse('0')
                                : int.parse(ageInputController.text),
                            "težina": weightInputController.text.isEmpty
                                ? int.parse('0')
                                : int.parse(weightInputController.text),
                            "visina": heightInputController.text.isEmpty
                                ? int.parse('0')
                                : int.parse(heightInputController.text),
                            "konj": this.horse
                          })
                          .then((result) => {
                                Navigator.pop(context),
                                nameInputController.clear(),
                                surnameInputController.clear(),
                                ageInputController.clear(),
                                heightInputController.clear(),
                                weightInputController.clear()
                              })
                          .catchError((err) => print(err));
                    }
                  },
                )
              ],
            ),
          )),
    );
  }
}
