import 'package:flutter/material.dart';
import 'package:new_project/screens/horses/specificHorse.dart';
import 'package:new_project/screens/horses/treatmentOfHorses/horseMedicine.dart';
import 'package:new_project/screens/horses/treatmentOfHorses/horseVaccination.dart';
import 'package:new_project/screens/horses/treatmentOfHorses/veterinarianVisits.dart';

class PickTypeOfTreatment extends StatefulWidget {
  final String horseId;
  final String name;

  PickTypeOfTreatment({@required this.horseId, @required this.name});
  @override
  _PickTypeOfTreatmentState createState() =>
      _PickTypeOfTreatmentState(name: this.name, horseId: this.horseId);
}

class _PickTypeOfTreatmentState extends State<PickTypeOfTreatment> {
  final String horseId;
  final String name;

  _PickTypeOfTreatmentState({@required this.horseId, @required this.name});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.brown[400],
          title: Text('Liječenja - ' + this.name),
        ),
        body: Container(
            child: Column(children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Material(
                  child: GestureDetector(
                    child: Container(
                      color: Colors.brown[100],
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.29,
                      child: FittedBox(
                        child: Column(
                          children: [
                            Image.asset('dizajn/veterinar.png'),
                            Text('Posjeti veterinara',
                                style: TextStyle(
                                    fontSize: 80.0, color: Colors.brown[400]))
                          ],
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VetTreatments(
                                name: this.name, horseId: this.horseId),
                          ));
                    },
                  ),
                )
              ]),
          Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Material(
                  child: GestureDetector(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.29,
                      child: FittedBox(
                        child: Column(
                          children: [
                            Image.asset('dizajn/cijepljenja.png'),
                            Text('Cijepljenja',
                                style: TextStyle(
                                    fontSize: 60.0, color: Colors.brown[400]))
                          ],
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => HorseVaccination(
                                name: this.name, horseId: this.horseId),
                          ));
                    },
                  ),
                )
              ]),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Material(
                child: GestureDetector(
                  child: Container(
                    color: Colors.brown[100],
                    padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.29,
                    child: FittedBox(
                      child: Column(
                        children: [
                          Image.asset('dizajn/lijekovi.png'),
                          Text('Lijekovi',
                              style: TextStyle(
                                  fontSize: 80.0, color: Colors.brown[400]))
                        ],
                      ),
                      fit: BoxFit.fill,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => HorseMedicine(
                              name: this.name, horseId: this.horseId),
                        ));
                  },
                ),
              )
            ],
          ),
        ])));
  }
}
