import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/horses/addNewHorse.dart';
import 'package:new_project/screens/horses/specificHorse.dart';

class ListHorses extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Popis konja/kobila'),
        backgroundColor: Colors.brown[400],
      ),
      body: new StreamBuilder(
        stream: FirebaseFirestore.instance.collection('konji').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Text(document['ime']),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SpecificHorse(
                                name: document['ime'], horseId: document.id)));
                  },
                  onLongPress: () async {
                    await showDialog(
                        context: context,
                        child: new AlertDialog(
                          content: new Text(
                            'Želite li obrisati konja/kobilu?',
                            style: TextStyle(color: Colors.brown),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Odustani',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              textColor: Colors.white,
                              color: Colors.brown[400],
                              child: Text('Obriši'),
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('konji')
                                    .doc(document.id)
                                    .delete();
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ));
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddNewHorse(),
              ));
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
