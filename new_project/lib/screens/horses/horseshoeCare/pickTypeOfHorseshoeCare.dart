import 'package:flutter/material.dart';
import 'package:new_project/screens/horses/horseshoeCare/hoofProcessing.dart';
import 'package:new_project/screens/horses/horseshoeCare/horseshoeing.dart';

class PickTypeOfHorseshoeCare extends StatefulWidget {
  final String horseId;
  final String name;

  PickTypeOfHorseshoeCare({@required this.horseId, @required this.name});
  @override
  _PickTypeOfHorseshoeCareState createState() =>
      _PickTypeOfHorseshoeCareState(name: this.name, horseId: this.horseId);
}

class _PickTypeOfHorseshoeCareState extends State<PickTypeOfHorseshoeCare> {
  final String horseId;
  final String name;

  _PickTypeOfHorseshoeCareState({@required this.horseId, @required this.name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.brown[400],
        title: Text('Briga o kopitima - ' + this.name),
      ),
      body: Container(
        color: Colors.brown[100],
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Material(
                  child: GestureDetector(
                    child: Container(
                        color: Colors.brown[100],
                        padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height / 2.4,
                        child: FittedBox(
                          child: Column(
                            children: [
                              Image.asset('dizajn/potkivanje.png'),
                              Text(
                                'Potkivanje',
                                style: TextStyle(
                                    fontSize: 90.0, color: Colors.brown[400]),
                              )
                            ],
                          ),
                          fit: BoxFit.fill,
                        )),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => Horseshoeing(
                              horseId: this.horseId, name: this.name)));
                    },
                  ),
                )
              ],
            ),
            Text(''),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Material(
                  child: GestureDetector(
                    child: Container(
                      color: Colors.brown[100],
                      padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 10.0),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 2.4,
                      child: FittedBox(
                        child: Column(
                          children: [
                            Image.asset('dizajn/obradaKopita.png'),
                            Text(
                              'Obrada kopita',
                              style: TextStyle(
                                  fontSize: 80.0, color: Colors.brown[400]),
                            ),
                          ],
                        ),
                        fit: BoxFit.fill,
                      ),
                    ),
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (BuildContext context) => HoofProcessing(
                              horseId: this.horseId, name: this.name)));
                    },
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
