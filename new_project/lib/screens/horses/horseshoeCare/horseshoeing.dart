import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/shared/methods.dart';

class Horseshoeing extends StatefulWidget {
  final String horseId;
  final String name;

  Horseshoeing({@required this.horseId, @required this.name});

  @override
  _HorseshoeingState createState() =>
      _HorseshoeingState(name: this.name, horseId: this.horseId);
}

class _HorseshoeingState extends State<Horseshoeing> {
  final String horseId;
  final String name;

  var finalDate;
  TextEditingController descriptionInputController;
  String n;

  _HorseshoeingState({@required this.horseId, @required this.name});

  @override
  void initState() {
    descriptionInputController = new TextEditingController();
    super.initState();
  }

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finalDate = order;
    });
  }

  @override
  Widget build(BuildContext context) {
    _showDialog(numOfLegs) async {
      List<String> legs = numOfLegs;
      setState(() {
        n = legs.first;
      });
      await showDialog(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                            labelText: 'Opis*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown))),
                        controller: descriptionInputController,
                      ),
                    ),
                    Row(
                      children: [
                        Text(
                          'Potkovano nogu: ',
                          style: TextStyle(color: Colors.brown),
                        ),
                        Expanded(
                          child: DropdownButton<String>(
                            value: this.n,
                            isDense: true,
                            onChanged: (String newValue) {
                              setState(() {
                                this.n = newValue;
                              });
                            },
                            items: legs.map((value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(
                                  value,
                                  style: TextStyle(color: Colors.brown),
                                ),
                              );
                            }).toList(),
                          ),
                        )
                      ],
                    ),
                    new RaisedButton(
                      onPressed: callDatePicker,
                      color: Colors.brown[300],
                      child: new Text(
                        'Odaberi datum',
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  descriptionInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.brown[400],
                child: Text(
                  'Dodaj',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (finalDate != null) {
                    FirebaseFirestore.instance
                        .collection('konjiPotkivanje')
                        .add({
                          "opis": descriptionInputController.text.isEmpty
                              ? ''
                              : descriptionInputController.text,
                          "datum": finalDate,
                          "konj": this.horseId,
                          "potkovano": this.n
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              descriptionInputController.clear()
                            })
                        .catchError((err) => print(err));
                  }
                },
              )
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Potkivanja - ' + this.name),
        backgroundColor: Colors.brown[400],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('konjiPotkivanje')
            .where('konj', isEqualTo: this.horseId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Text(timestampToString(document['datum'])),
                  onTap: () {
                    showDialog(
                        context: context,
                        child: AlertDialog(
                          content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text('Potkovano: ' + document['potkovano']),
                              new Text('Opis: ' + document['opis'])
                            ],
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Izađi',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ));
                  },
                  onLongPress: () async {
                    await showDialog(
                        context: context,
                        child: AlertDialog(
                          content: new Text(
                            'Želite li obrisati sadržaj?',
                            style: TextStyle(color: Colors.brown),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Odustani',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              textColor: Colors.white,
                              color: Colors.brown[400],
                              child: Text('Obriši'),
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('konjiPotkivanje')
                                    .doc(document.id)
                                    .delete();
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ));
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () async {
          List<String> numOfLegs = new List();
          await FirebaseFirestore.instance
              .collection('brojPotkovanihNogu')
              .get()
              .then((querySnapshot) {
            querySnapshot.docs.forEach((element) {
              numOfLegs.add(element['potkovano']);
            });
            _showDialog(numOfLegs);
          });
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
