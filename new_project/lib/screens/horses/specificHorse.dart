import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/horses/horseTraining/horseTraining.dart';
import 'package:new_project/screens/horses/horseshoeCare/pickTypeOfHorseshoeCare.dart';
import 'package:new_project/screens/horses/treatmentOfHorses/pickTypeOfTreatment.dart';

class SpecificHorse extends StatefulWidget {
  final String name;
  final String horseId;

  SpecificHorse({@required this.name, @required this.horseId});
  @override
  _SpecificHorseState createState() =>
      _SpecificHorseState(name: this.name, horseId: this.horseId);
}

class _SpecificHorseState extends State<SpecificHorse> {
  final String name;
  final String horseId;
  _SpecificHorseState({this.name, this.horseId});

  TextEditingController ageInputController;
  TextEditingController weightInputController;
  TextEditingController heightInputController;
  TextEditingController speciesInputController;

  @override
  void initState() {
    ageInputController = new TextEditingController();
    heightInputController = new TextEditingController();
    weightInputController = new TextEditingController();
    speciesInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() {
      showDialog<String>(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                            labelText: 'Dob*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown))),
                        controller: ageInputController,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Visina*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown))),
                        controller: heightInputController,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        keyboardType: TextInputType.number,
                        autofocus: true,
                        decoration: InputDecoration(
                            labelText: 'Težina*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown))),
                        controller: weightInputController,
                      ),
                    ),
                    Expanded(
                      child: TextField(
                        autofocus: true,
                        decoration: InputDecoration(
                            labelText: 'Vrsta*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown))),
                        controller: speciesInputController,
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text('Spremi promjene'),
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection("konji")
                      .doc(this.horseId)
                      .set({
                        "dob": ageInputController.text.isEmpty
                            ? int.parse('0')
                            : int.parse(ageInputController.text),
                        "visina": heightInputController.text.isEmpty
                            ? int.parse('0')
                            : int.parse(heightInputController.text),
                        "težina": weightInputController.text.isEmpty
                            ? int.parse('0')
                            : int.parse(weightInputController.text),
                        "vrsta": speciesInputController.text.isEmpty
                            ? ""
                            : speciesInputController.text,
                        "ime": this.name
                      })
                      .then((result) => {
                            Navigator.pop(context),
                          })
                      .catchError((err) => print(err));
                },
              )
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(this.name),
        backgroundColor: Colors.brown[400],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text('Liječenje'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PickTypeOfTreatment(
                            horseId: this.horseId, name: this.name)));
              },
            ),
            ListTile(
              title: Text('Briga o kopitima'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => PickTypeOfHorseshoeCare(
                          horseId: this.horseId, name: this.name),
                    ));
              },
            ),
            ListTile(
              title: Text('Treninzi'),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ListHorseTraining(
                            horseId: this.horseId, name: this.name)));
              },
            )
          ],
        ),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('konji')
            .doc(this.horseId)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return new Text('Trenutak!');
          }
          var horse = snapshot.data;
          this.ageInputController =
              new TextEditingController(text: horse['dob'].toString());
          this.heightInputController =
              new TextEditingController(text: horse['visina'].toString());
          this.weightInputController =
              new TextEditingController(text: horse['težina'].toString());
          this.speciesInputController =
              new TextEditingController(text: horse['vrsta'].toString());
          return Container(
            padding: EdgeInsets.all(20.0),
            child: Table(
              border: TableBorder.all(color: Colors.white),
              children: [
                TableRow(children: [
                  Text('Dob:'),
                  Text(
                    horse['dob'].toString() + " god",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ]),
                TableRow(children: [
                  Text('Visina:'),
                  Text(
                    horse['visina'].toString() + " cm",
                    style: Theme.of(context).textTheme.bodyText1,
                  )
                ]),
                TableRow(children: [
                  Text('Težina:'),
                  Text(
                    horse['težina'].toString() + " kg",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ]),
                TableRow(children: [
                  Text('Vrsta:'),
                  Text(
                    horse['vrsta'],
                    style: Theme.of(context).textTheme.bodyText1,
                  )
                ])
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () {
          _showDialog();
        },
        tooltip: 'Izmijeni',
        child: Icon(Icons.edit),
      ),
    );
  }
}
