import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AddNewHorse extends StatefulWidget {
  @override
  _AddNewHorseState createState() => _AddNewHorseState();
}

class _AddNewHorseState extends State<AddNewHorse> {
  TextEditingController nameInputController;
  TextEditingController ageInputController;
  TextEditingController weightInputController;
  TextEditingController heightInputController;
  TextEditingController speciesInputController;

  @override
  void initState() {
    nameInputController = new TextEditingController();
    ageInputController = new TextEditingController();
    heightInputController = new TextEditingController();
    weightInputController = new TextEditingController();
    speciesInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj konja/kobilu'),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Ime',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: nameInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.number,
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Dob',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: ageInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'Visina',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: heightInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        hintText: 'Težina',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: weightInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Vrsta',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: speciesInputController,
                    ),
                  ),
                ],
              ),
              FlatButton(
                color: Colors.brown[100],
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  nameInputController.clear();
                  ageInputController.clear();
                  weightInputController.clear();
                  heightInputController.clear();
                  speciesInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.brown[400],
                textColor: Colors.white,
                child: Text(
                  'Dodaj',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (!nameInputController.text.isNotEmpty) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: Container(
                                child: Container(
                                  child: Text('Potrebno je unijeti ime konja!'),
                                ),
                              ),
                            ));
                  }
                  if (nameInputController.text.isNotEmpty) {
                    FirebaseFirestore.instance
                        .collection('konji')
                        .add({
                          "ime": nameInputController.text,
                          "dob": ageInputController.text.isEmpty
                              ? int.parse('0')
                              : ageInputController.text,
                          "težina": weightInputController.text.isEmpty
                              ? int.parse('0')
                              : weightInputController.text,
                          "visina": heightInputController.text.isEmpty
                              ? int.parse('0')
                              : heightInputController.text,
                          "vrsta": speciesInputController.text.isEmpty
                              ? ""
                              : speciesInputController.text
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              nameInputController.clear(),
                              ageInputController.clear(),
                              weightInputController.clear(),
                              heightInputController.clear(),
                              speciesInputController.clear()
                            })
                        .catchError((err) => print(err));
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
