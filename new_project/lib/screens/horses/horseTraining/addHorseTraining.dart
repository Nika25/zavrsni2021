import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/horses/horseTraining/horseTraining.dart';
import 'package:new_project/shared/methods.dart';

class AddNewTraining extends StatefulWidget {
  final String horseId;
  final String name;
  final List<String> typesOfTraining;

  AddNewTraining(
      {@required this.horseId,
      @required this.name,
      @required this.typesOfTraining});

  @override
  _AddNewTrainingState createState() => _AddNewTrainingState(
      horseId: this.horseId,
      name: this.name,
      typesOfTraining: this.typesOfTraining);
}

class _AddNewTrainingState extends State<AddNewTraining> {
  final String horseId;
  final String name;
  final List<String> typesOfTraining;

  _AddNewTrainingState(
      {@required this.horseId,
      @required this.name,
      @required this.typesOfTraining});

  TextEditingController durationInputController;
  TextEditingController goodNotesInputController;
  TextEditingController badNotesInputController;
  var finalDate;
  String type;

  @override
  void initState() {
    durationInputController = new TextEditingController();
    goodNotesInputController = new TextEditingController();
    badNotesInputController = new TextEditingController();
    type = typesOfTraining.first;
    super.initState();
  }

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finalDate = order;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj trening - ' + this.name),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.number,
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Trajanje*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown))),
                      controller: durationInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Treba poraditi na*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          )),
                      controller: badNotesInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Uočena napredak*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.brown),
                          ),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown))),
                      controller: goodNotesInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'Vrsta treninga: ',
                    style: TextStyle(color: Colors.brown),
                  ),
                  Expanded(
                    child: DropdownButton<String>(
                      value: this.type,
                      isDense: true,
                      onChanged: (String newValue) {
                        setState(() {
                          this.type = newValue;
                        });
                      },
                      items: typesOfTraining.map((value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(
                            value,
                            style: TextStyle(color: Colors.brown),
                          ),
                        );
                      }).toList(),
                    ),
                  ),
                ],
              ),
              RaisedButton(
                onPressed: callDatePicker,
                color: Colors.brown[300],
                child: new Text(
                  'Odaberi datum',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              FlatButton(
                color: Colors.brown[100],
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  durationInputController.clear();
                  goodNotesInputController.clear();
                  badNotesInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.brown[400],
                textColor: Colors.white,
                child: Text(
                  'Dodaj',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  if (finalDate == null) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: Container(
                                child: Container(
                                  child: Text('Potrebno je odrediti datum'),
                                ),
                              ),
                            ));
                  }
                  if (finalDate != null) {
                    FirebaseFirestore.instance
                        .collection('konjiTrening')
                        .add({
                          "konj": this.horseId,
                          "datum": this.finalDate,
                          "vrsta": this.type,
                          "trajanje": durationInputController.text.isEmpty
                              ? int.parse('0')
                              : durationInputController.text,
                          "napredak": goodNotesInputController.text.isEmpty
                              ? ''
                              : goodNotesInputController.text,
                          "trebaVježbati": badNotesInputController.text.isEmpty
                              ? ''
                              : badNotesInputController.text
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              durationInputController.clear(),
                              badNotesInputController.clear(),
                              goodNotesInputController.clear(),
                            })
                        .catchError((err) => print(err));
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
