import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/horses/horseTraining/addHorseTraining.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/addNewTUser.dart';
import 'package:new_project/shared/methods.dart';

class ListHorseTraining extends StatelessWidget {
  final String horseId;
  final String name;

  ListHorseTraining({@required this.horseId, @required this.name});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Treninzi - ' + this.name),
        backgroundColor: Colors.brown[400],
      ),
      body: new StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('konjiTrening')
            .where('konj', isEqualTo: this.horseId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Text(timestampToString(document['datum']) +
                      " " +
                      document['vrsta']),
                  onTap: () async {
                    await showDialog(
                        context: context,
                        child: AlertDialog(
                          content: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Vrsta: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text(document['vrsta']),
                              new Text(
                                'Treba raditi na: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                document['trebaVježbati'],
                              ),
                              new Text(
                                'Uočen napredak: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text(document['napredak']),
                              new Text(
                                'Trajanje: ',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              new Text(
                                document['trajanje'].toString() + " min",
                              )
                            ],
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Izađi',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ));
                  },
                  onLongPress: () async {
                    await showDialog(
                        context: context,
                        child: AlertDialog(
                          content: new Text(
                            'Želite li obrisati sadržaj?',
                            style: TextStyle(color: Colors.brown),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Odustani',
                                style: TextStyle(
                                  color: Colors.brown,
                                ),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                              textColor: Colors.white,
                              color: Colors.brown[400],
                              child: Text(
                                'Obriši',
                              ),
                              onPressed: () {
                                FirebaseFirestore.instance
                                    .collection('konjiTrening')
                                    .doc(document.id)
                                    .delete();
                                Navigator.pop(context);
                              },
                            )
                          ],
                        ));
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () async {
          List<String> typesOfTraining = new List();
          await FirebaseFirestore.instance
              .collection('vrsteTreninga')
              .get()
              .then((querySnapshot) {
            querySnapshot.docs.forEach((element) {
              typesOfTraining.add(element['vrsta']);
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => AddNewTraining(
                        horseId: this.horseId,
                        name: this.name,
                        typesOfTraining: typesOfTraining)));
          });
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
