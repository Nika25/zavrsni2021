import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/home/home.dart';
import 'package:new_project/services/auth.dart';
import 'package:new_project/shared/constants.dart';
import 'package:new_project/shared/loading.dart';

class AddTUser extends StatefulWidget {
  @override
  _AddTUserState createState() => _AddTUserState();
}

class _AddTUserState extends State<AddTUser> {
  TextEditingController nameInputController;
  TextEditingController surnameInputController;
  TextEditingController ageInputController;
  TextEditingController diagnoseInputController;
  bool _isChecked = false;

  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    nameInputController = new TextEditingController();
    surnameInputController = new TextEditingController();
    ageInputController = new TextEditingController();
    diagnoseInputController = new TextEditingController();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj korisnika'),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Ime',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: nameInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Prezime',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: surnameInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Dob',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: ageInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                        hintText: 'Dijagnoza',
                        labelStyle: TextStyle(color: Colors.brown),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.brown),
                        ),
                      ),
                      controller: diagnoseInputController,
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Checkbox(
                    value: this._isChecked,
                    onChanged: (value) {
                      setState(() {
                        this._isChecked = !this._isChecked;
                      });
                    },
                  ),
                  Expanded(
                      child: Text(
                    'Predana izjava liječnika o kontraindikacijama.',
                    style: TextStyle(fontSize: 18.0),
                  ))
                ],
              ),
              FlatButton(
                  color: Colors.brown[100],
                  child: Text(
                    'Odustani',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    nameInputController.clear();
                    surnameInputController.clear();
                    ageInputController.clear();
                    diagnoseInputController.clear();
                    Navigator.pop(context);
                  }),
              FlatButton(
                color: Colors.brown[400],
                textColor: Colors.white,
                child: Text('Dodaj', style: TextStyle(color: Colors.white)),
                onPressed: () {
                  if (!nameInputController.text.isNotEmpty ||
                      !surnameInputController.text.isNotEmpty) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: Container(
                                child:
                                    Text('Potrebno je unijeti ime i prezime!'),
                              ),
                            ));
                  }
                  if (nameInputController.text.isNotEmpty &&
                      surnameInputController.text.isNotEmpty)
                    FirebaseFirestore.instance
                        .collection("korisnici")
                        .add({
                          "ime": nameInputController.text,
                          "prezime": surnameInputController.text,
                          "dob": ageInputController.text.isEmpty
                              ? int.parse('0')
                              : int.parse(ageInputController.text),
                          "dijagnoza": diagnoseInputController.text.isEmpty
                              ? 'nepoznata'
                              : diagnoseInputController.text,
                          "izjavaKontraindikacije": this._isChecked
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              nameInputController.clear(),
                              surnameInputController.clear(),
                              ageInputController.clear(),
                              diagnoseInputController.clear(),
                            })
                        .catchError((err) => print(err));
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
