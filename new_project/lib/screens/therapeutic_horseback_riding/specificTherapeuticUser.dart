import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/notesOfTUser.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/paymentOfTUser.dart';

import 'listTherapeuticUsers.dart';

class SpecificTherapeuticUser extends StatefulWidget {
  final String name;
  final String surname;
  final String therapeuticUserId;

  SpecificTherapeuticUser(
      {@required this.name,
      @required this.surname,
      @required this.therapeuticUserId});

  @override
  _SpecificTherapeuticUserState createState() => _SpecificTherapeuticUserState(
      name: this.name,
      surname: this.surname,
      therapeuticUserId: this.therapeuticUserId);
}

class _SpecificTherapeuticUserState extends State<SpecificTherapeuticUser> {
  final String name;
  final String surname;
  final String therapeuticUserId;

  TextEditingController diagnoseController;
  TextEditingController ageController;
  TextEditingController contraindicationController;

  _SpecificTherapeuticUserState(
      {@required this.name,
      @required this.surname,
      @required this.therapeuticUserId});

  @override
  void initState() {
    diagnoseController = new TextEditingController();
    ageController = new TextEditingController();
    contraindicationController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() {
      showDialog<String>(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'Dijagnoza*',
                                labelStyle: TextStyle(color: Colors.brown),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown))),
                            controller: diagnoseController,
                          ),
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'Dob*',
                                labelStyle: TextStyle(color: Colors.brown),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown))),
                            controller: ageController,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text('Spremi promjene'),
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection('korisnici')
                      .doc(this.therapeuticUserId)
                      .set({
                        "dijagnoza": diagnoseController.text.isEmpty
                            ? ''
                            : diagnoseController.text,
                        "dob":
                            ageController.text.isEmpty ? 0 : ageController.text,
                        "ime": this.name,
                        "prezime": this.surname,
                        "izjavaKontraindikacije":
                            contraindicationController.text == 'true'
                                ? true
                                : false,
                      })
                      .then((result) => {
                            Navigator.pop(context),
                          })
                      .catchError((err) => print(err));
                },
              ),
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(this.name + " " + this.surname),
        backgroundColor: Colors.brown[400],
      ),
      drawer: Drawer(
          child: ListView(
        children: <Widget>[
          ListTile(
            title: Text("Plaćanja"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => PaymentTUser(
                        therapeuticUserId: this.therapeuticUserId,
                        name: this.name,
                        surname: this.surname,
                      )));
            },
          ),
          ListTile(
            title: Text("Bilješke"),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => NotesTUser(
                        therapeuticUserId: this.therapeuticUserId,
                        name: this.name,
                        surname: this.surname,
                      )));
            },
          ),
        ],
      )),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('korisnici')
            .doc(this.therapeuticUserId)
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return new Text('Trenutak!');
          }
          var tUser = snapshot.data;
          this.ageController =
              new TextEditingController(text: tUser['dob'].toString());
          this.diagnoseController =
              new TextEditingController(text: tUser['dijagnoza'].toString());
          this.contraindicationController = new TextEditingController(
              text: tUser['izjavaKontraindikacije'].toString());
          return Container(
              padding: EdgeInsets.all(20.0),
              child: Table(
                border: TableBorder.all(color: Colors.white),
                children: [
                  TableRow(children: [
                    Text('Dijagnoza:'),
                    Text(
                      tUser['dijagnoza'],
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]),
                  TableRow(children: [
                    Text('Dob:'),
                    Text(
                      tUser['dob'].toString(),
                      style: Theme.of(context).textTheme.bodyText1,
                    )
                  ]),
                  TableRow(
                    children: [
                      Text('Izjava liječnika:'),
                      Text(
                        tUser['izjavaKontraindikacije'].toString() == 'true'
                            ? 'DA'
                            : 'NE',
                        style: Theme.of(context).textTheme.bodyText1,
                      )
                    ],
                  )
                ],
              ));
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () {
          _showDialog();
        },
        tooltip: 'Izmijeni',
        child: Icon(Icons.edit),
      ),
    );
  }
}
