import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/shared/methods.dart';

class NotesTUser extends StatefulWidget {
  final String therapeuticUserId;
  final String name;
  final String surname;

  NotesTUser(
      {@required this.therapeuticUserId,
      @required this.name,
      @required this.surname});

  @override
  _NotesTUserState createState() => _NotesTUserState(
      therapeuticUserId: this.therapeuticUserId,
      name: this.name,
      surname: this.surname);
}

class _NotesTUserState extends State<NotesTUser> {
  final String therapeuticUserId;
  final String name;
  final String surname;

  TextEditingController notesDescritpionInputController;
  TextEditingController notesDurationInputController;
  TextEditingController notesHorseInputController;

  _NotesTUserState(
      {@required this.therapeuticUserId,
      @required this.name,
      @required this.surname});

  var finaldate;

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finaldate = order;
    });
  }

  @override
  void initState() {
    notesDescritpionInputController = new TextEditingController();
    notesDurationInputController = new TextEditingController();
    notesHorseInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() async {
      await showDialog<String>(
          context: context,
          child: AlertDialog(
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: 'Konj',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                            ),
                            controller: notesHorseInputController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: 'Trajanje',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                            ),
                            controller: notesDurationInputController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                              hintText: 'Opis',
                              labelStyle: TextStyle(color: Colors.brown),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.brown),
                              ),
                            ),
                            controller: notesDescritpionInputController,
                          ),
                        ),
                      ],
                    ),
                    new RaisedButton(
                      onPressed: callDatePicker,
                      color: Colors.brown[400],
                      child: new Text(
                        "Odaberi datum",
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                  child: Text(
                    'Odustani',
                    style: TextStyle(color: Colors.brown),
                  ),
                  onPressed: () {
                    notesDescritpionInputController.clear();
                    notesDurationInputController.clear();
                    notesHorseInputController.clear();
                    Navigator.pop(context);
                  }),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text('Dodaj'),
                onPressed: () {
                  if (notesDurationInputController.text.isNotEmpty &&
                      notesHorseInputController.text.isNotEmpty &&
                      finaldate != null)
                    FirebaseFirestore.instance
                        .collection("biljeskeKorisnici")
                        .add({
                          "korisnik": therapeuticUserId,
                          "datum": finaldate,
                          "trajanje":
                              int.parse(notesDurationInputController.text),
                          "opis": notesDescritpionInputController.text,
                          "konj": notesHorseInputController.text
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              notesDescritpionInputController.clear(),
                              notesDurationInputController.clear(),
                              notesHorseInputController.clear()
                            })
                        .catchError((err) => print(err));
                },
              )
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Bilješke - " + this.name + " " + this.surname),
        backgroundColor: Colors.brown[400],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('biljeskeKorisnici')
            .where('korisnik', isEqualTo: this.therapeuticUserId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }

          return ListView(
            children: snapshot.data.docs.map((document) {
              return document['korisnik'].toString() ==
                      this.therapeuticUserId.toString()
                  ? Card(
                      child: ListTile(
                        title: Text(timestampToString(document['datum'])),
                        onTap: () async {
                          await showDialog(
                              context: context,
                              child: new AlertDialog(
                                content: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Text('Konj: ' + document['konj']),
                                    new Text('Trajanje: ' +
                                        document['trajanje'].toString() +
                                        " minuta"),
                                    new Text('Opis: ' + document['opis'])
                                  ],
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                      child: Text(
                                        'Izađi',
                                        style: TextStyle(color: Colors.brown),
                                      ),
                                      onPressed: () {
                                        Navigator.pop(context);
                                      }),
                                ],
                              ));
                        },
                        onLongPress: () async {
                          await showDialog(
                              context: context,
                              child: new AlertDialog(
                                content: new Text(
                                  'Želite li obrisati bilješku?',
                                  style: TextStyle(
                                    color: Colors.brown,
                                  ),
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'Odustani',
                                      style: TextStyle(
                                        color: Colors.brown,
                                      ),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  FlatButton(
                                    textColor: Colors.white,
                                    color: Colors.brown[400],
                                    child: Text('Obriši'),
                                    onPressed: () {
                                      FirebaseFirestore.instance
                                          .collection('biljeskeKorisnici')
                                          .doc(document.id)
                                          .delete();
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ));
                        },
                      ),
                    )
                  : Text('');
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: _showDialog,
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
