import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'package:new_project/shared/methods.dart';

class PaymentTUser extends StatefulWidget {
  final String therapeuticUserId;
  final String name;
  final String surname;
  PaymentTUser(
      {@required this.therapeuticUserId,
      @required this.name,
      @required this.surname});

  @override
  _PaymentTUserState createState() => _PaymentTUserState(
      therapeuticUserId: this.therapeuticUserId,
      name: this.name,
      surname: this.surname);
}

class _PaymentTUserState extends State<PaymentTUser> {
  final String therapeuticUserId;
  final String name;
  final String surname;
  TextEditingController paymentCostInputController;

  _PaymentTUserState(
      {@required this.therapeuticUserId,
      @required this.name,
      @required this.surname});
  var finaldate;

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finaldate = order;
    });
  }

  @override
  void initState() {
    paymentCostInputController = new TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() async {
      await showDialog<String>(
        context: context,
        child: AlertDialog(
          contentPadding: const EdgeInsets.all(16.0),
          content: Container(
            child: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Row(
                    children: [
                      Expanded(
                        child: TextField(
                          keyboardType: TextInputType.number,
                          autofocus: true,
                          decoration: InputDecoration(
                            labelText: 'Iznos u kunama*',
                            labelStyle: TextStyle(color: Colors.brown),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown),
                            ),
                          ),
                          controller: paymentCostInputController,
                        ),
                      ),
                    ],
                  ),
                  new RaisedButton(
                    onPressed: callDatePicker,
                    color: Colors.brown[400],
                    child: new Text(
                      "Odaberi datum",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ),
          actions: <Widget>[
            FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.brown),
                ),
                onPressed: () {
                  paymentCostInputController.clear();
                  Navigator.pop(context);
                }),
            FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text(
                  'Dodaj',
                ),
                onPressed: () {
                  if (paymentCostInputController.text.isNotEmpty &&
                      finaldate != null) {
                    FirebaseFirestore.instance
                        .collection("placanjaKorisnici")
                        .add({
                          "iznos": int.parse(paymentCostInputController.text),
                          "datum": finaldate,
                          "korisnik": therapeuticUserId
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              paymentCostInputController.clear(),
                            })
                        .catchError((err) => print(err));
                  }
                })
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Plaćanja - " + this.name + " " + this.surname),
        backgroundColor: Colors.brown[400],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('placanjaKorisnici')
            .where('korisnik', isEqualTo: this.therapeuticUserId)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return document['korisnik'].toString() ==
                      this.therapeuticUserId.toString()
                  ? Card(
                      child: ListTile(
                        title: Text(document['iznos'].toString() +
                            ' ' +
                            'kn, ' +
                            timestampToString(document['datum'])),
                        onTap: () async {
                          await showDialog(
                            context: context,
                            child: new AlertDialog(
                              content: new Text(
                                "Želite li obrisati sadržaj?",
                                style: TextStyle(
                                  color: Colors.brown,
                                ),
                              ),
                              actions: <Widget>[
                                FlatButton(
                                    child: Text(
                                      'Odustani',
                                      style: TextStyle(color: Colors.brown),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    }),
                                FlatButton(
                                    textColor: Colors.white,
                                    color: Colors.brown[400],
                                    child: Text(
                                      'Obriši',
                                    ),
                                    onPressed: () {
                                      FirebaseFirestore.instance
                                          .collection("placanjaKorisnici")
                                          .doc(document.id)
                                          .delete();
                                      Navigator.pop(context);
                                    })
                              ],
                            ),
                          );
                        },
                      ),
                    )
                  : Text('');
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: _showDialog,
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
