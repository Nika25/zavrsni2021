import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:new_project/screens/info/info_page2.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/addNewTUser.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/specificTherapeuticUser.dart';

class ListTherapeuticUsers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Korisnici"),
        backgroundColor: Colors.brown[400],
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance.collection('korisnici').snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }

          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Text(document['ime'] + ' ' + document['prezime']),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SpecificTherapeuticUser(
                                name: document['ime'],
                                surname: document['prezime'],
                                therapeuticUserId: document.id)));
                  },
                  onLongPress: () async {
                    await showDialog(
                      context: context,
                      child: new AlertDialog(
                        content: new Text(
                          'Želite li obrisati korisnika?',
                          style: TextStyle(
                            color: Colors.brown,
                          ),
                        ),
                        actions: <Widget>[
                          FlatButton(
                            child: Text(
                              'Odustani',
                              style: TextStyle(color: Colors.brown),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                          FlatButton(
                            textColor: Colors.white,
                            color: Colors.brown[400],
                            child: Text(
                              'Obriši',
                            ),
                            onPressed: () {
                              FirebaseFirestore.instance
                                  .collection('korisnici')
                                  .doc(document.id)
                                  .delete();
                              Navigator.pop(context);
                            },
                          )
                        ],
                      ),
                    );
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddTUser()));
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
