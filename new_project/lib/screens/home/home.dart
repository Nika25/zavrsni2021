import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:new_project/screens/home/initialPage.dart';
import 'package:new_project/screens/horses/listHorses.dart';
import 'package:new_project/screens/info/info_page2.dart';
import 'package:new_project/screens/recreationalRiding/listVisits.dart';
import 'package:new_project/screens/school_of_horseback_riding/listRidingStudents.dart';
import 'package:new_project/screens/therapeutic_horseback_riding/listTherapeuticUsers.dart';
import 'package:new_project/services/auth.dart';
import 'package:new_project/services/database.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/info/info_page.dart';

class HomePage extends StatefulWidget {
  @override
  Home createState() => Home();
}

class Home extends State<HomePage> {
  final AuthService _auth = AuthService();
  int index;
  List<Widget> tabs;
  var _pageController;
  Home() {
    tabs = [new EventsOnCalendar(), new AssociationInfo()];
    index = 0;
    _pageController = PageController(initialPage: index);
  }
  @override
  Widget build(BuildContext context) {
    return StreamProvider<QuerySnapshot>.value(
        value: DatabaseService().users,
        child: Scaffold(
          backgroundColor: Colors.brown[50],
          appBar: AppBar(
            title: Text("Saddle Up!"),
            backgroundColor: Colors.brown[400],
            elevation: 0.0,
            actions: <Widget>[
              FlatButton.icon(
                icon: Icon(
                  Icons.person,
                  color: Colors.white,
                ),
                label: Text(
                  'Odjavi se',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  await _auth.signOut();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Initial()));
                },
              ),
            ],
          ),
          drawer: Drawer(
              child: ListView(
            children: <Widget>[
              ListTile(
                title: Text("Terapijsko jahanje"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ListTherapeuticUsers()));
                },
              ),
              ListTile(
                title: Text("Škola jahanja"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => ListRidingStudents()));
                },
              ),
              ListTile(
                title: Text("Rekreativno jahanje"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => ListVisits()));
                },
              ),
              ListTile(
                title: Text("Konji"),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => ListHorses()));
                },
              )
            ],
          )),
          body: PageView(
            children: tabs,
            onPageChanged: (i) {
              setState(() {
                index = i;
              });
            },
            controller: _pageController,
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: index,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(Icons.calendar_today, color: Colors.brown[400]),
                  label: 'Kalendar',
                  backgroundColor: Colors.brown[900]),
              BottomNavigationBarItem(
                  icon: Icon(Icons.info),
                  label: 'Informacije',
                  backgroundColor: Colors.blue)
            ],
            onTap: (i) {
              setState(() {
                index = i;
                _pageController.animateToPage(index,
                    duration: Duration(microseconds: 200),
                    curve: Curves.linear);
              });
            },
          ),
        ));
  }
}
