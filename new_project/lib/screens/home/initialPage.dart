import 'package:flutter/material.dart';
import 'package:new_project/screens/authenticate/sign_in.dart';

class Initial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Row(),
            Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Image.asset(
                    'dizajn/initialPage.png',
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.85,
                  ),
                ],
              ),
            ),
            ElevatedButton(
                child: Text(
                  "Prijava",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w400),
                ),
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignIn()));
                }),
          ],
        ));
  }
}
