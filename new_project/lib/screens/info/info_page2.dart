import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AssociationInfo extends StatefulWidget {
  @override
  _AssociationInfoState createState() => _AssociationInfoState();
}

class _AssociationInfoState extends State<AssociationInfo> {
  TextEditingController oibController;
  TextEditingController adressController;

  @override
  void initState() {
    oibController = new TextEditingController();
    adressController = new TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    _showDialog() {
      showDialog<String>(
          context: context,
          child: AlertDialog(
            contentPadding: const EdgeInsets.all(16.0),
            content: Container(
              child: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            keyboardType: TextInputType.number,
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'OIB*',
                                labelStyle: TextStyle(color: Colors.brown),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown))),
                            controller: oibController,
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: TextField(
                            autofocus: true,
                            decoration: InputDecoration(
                                labelText: 'Adresa*',
                                labelStyle: TextStyle(color: Colors.brown),
                                enabledBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.brown))),
                            controller: adressController,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Odustani',
                  style: TextStyle(
                    color: Colors.brown,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                textColor: Colors.white,
                color: Colors.brown[400],
                child: Text('Spremi promjene'),
                onPressed: () {
                  FirebaseFirestore.instance
                      .collection('udruga')
                      .doc('ojreNeLIymYSweBR9C0e')
                      .set({
                        "ime": ' Grabarije-Knin',
                        "adresa": adressController.text.isEmpty
                            ? ''
                            : adressController.text,
                        "oib":
                            oibController.text.isEmpty ? '' : oibController.text
                      })
                      .then((result) => {
                            Navigator.pop(context),
                          })
                      .catchError((err) => print(err));
                },
              )
            ],
          ));
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Informacije o udruzi",
            style: TextStyle(color: Colors.brown[400])),
        backgroundColor: Colors.white,
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('udruga')
            .doc('ojreNeLIymYSweBR9C0e')
            .snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return new Text('Trenutak!');
          }
          var element = snapshot.data;
          this.adressController =
              new TextEditingController(text: element['adresa'].toString());
          this.oibController =
              new TextEditingController(text: element['oib'].toString());
          return Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                Row(children: [
                  Text(
                    'Naziv: ',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Text(element['ime'])
                ]),
                Row(children: [
                  Text(
                    'OIB: ',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Text(element['oib'])
                ]),
                Row(children: [
                  Text(
                    'Adresa: ',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  Text(element['adresa'])
                ]),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () {
          _showDialog();
        },
        tooltip: 'Izmijeni',
        child: Icon(Icons.edit),
      ),
    );
  }
}
