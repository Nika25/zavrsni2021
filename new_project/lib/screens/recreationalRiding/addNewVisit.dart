import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/shared/methods.dart';

class AddNewVisit extends StatefulWidget {
  final List<String> typesOfRiding;

  AddNewVisit({@required this.typesOfRiding});

  @override
  _AddNewVisitState createState() =>
      _AddNewVisitState(typesOfRiding: this.typesOfRiding);
}

class _AddNewVisitState extends State<AddNewVisit> {
  final List<String> typesOfRiding;

  _AddNewVisitState({@required this.typesOfRiding});

  TextEditingController descriptionInputController;
  TextEditingController priceInputController;

  var finalDate;
  String type;

  @override
  void initState() {
    descriptionInputController = new TextEditingController();
    priceInputController = new TextEditingController();

    type = typesOfRiding.first;
    super.initState();
  }

  void callDatePicker() async {
    var order = await getDate(context);
    setState(() {
      finalDate = order;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dodaj novi posjet'),
        backgroundColor: Colors.brown[400],
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: SingleChildScrollView(
          child: ListBody(
            children: <Widget>[
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      keyboardType: TextInputType.number,
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Cijena u kunama*',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown))),
                      controller: priceInputController,
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: TextField(
                      autofocus: true,
                      decoration: InputDecoration(
                          hintText: 'Npr. e-mail, broj, ime i sl. posjetitelja',
                          labelStyle: TextStyle(color: Colors.brown),
                          enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown)),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.brown))),
                      controller: descriptionInputController,
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  Text(
                    'Vrsta jahanja: ',
                    style: TextStyle(color: Colors.brown),
                  ),
                  Expanded(
                    child: DropdownButton<String>(
                      value: this.type,
                      isDense: true,
                      onChanged: (String newValue) {
                        setState(() {
                          this.type = newValue;
                        });
                      },
                      items: typesOfRiding.map((value) {
                        return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(
                              value,
                              style: TextStyle(color: Colors.brown),
                            ));
                      }).toList(),
                    ),
                  )
                ],
              ),
              FlatButton(
                onPressed: callDatePicker,
                color: Colors.brown[300],
                child: Text(
                  'Odaberi datum',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              Text(''),
              FlatButton(
                color: Colors.brown[100],
                child: Text(
                  'Odustani',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () {
                  descriptionInputController.clear();
                  priceInputController.clear();
                  Navigator.pop(context);
                },
              ),
              FlatButton(
                color: Colors.brown[400],
                textColor: Colors.white,
                child: Text(
                  'Dodaj',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                onPressed: () {
                  if (priceInputController.text.isEmpty || finalDate == null) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: Container(
                                child: Text(
                                    'Potrebno je unijeti cijenu i odabrati datum!'),
                              ),
                            ));
                  }
                  if (priceInputController.text.isNotEmpty &&
                      finalDate != null) {
                    FirebaseFirestore.instance
                        .collection('posjetiRekreativaca')
                        .add({
                          "bilješka": descriptionInputController.text.isEmpty
                              ? ''
                              : descriptionInputController.text,
                          "cijena": int.parse(priceInputController.text),
                          "datum": finalDate,
                          "vrsta": this.type
                        })
                        .then((result) => {
                              Navigator.pop(context),
                              descriptionInputController.clear(),
                              priceInputController.clear()
                            })
                        .catchError((err) => print(err));
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
