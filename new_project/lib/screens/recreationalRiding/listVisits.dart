import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/screens/recreationalRiding/addNewVisit.dart';
import 'package:new_project/shared/methods.dart';

class ListVisits extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Rekreativno jahanje'),
        backgroundColor: Colors.brown[400],
      ),
      body: new StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('posjetiRekreativaca')
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: Text('Trenutak!'),
            );
          }
          return ListView(
            children: snapshot.data.docs.map((document) {
              return Card(
                child: ListTile(
                  title: Row(
                    children: [
                      Text(
                        document['cijena'].toString() + 'kn, ',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text(timestampToString(document['datum']))
                    ],
                  ),
                  onTap: () async {
                    await showDialog(
                        context: context,
                        child: new AlertDialog(
                          content: Container(
                            child: SingleChildScrollView(
                              child: ListBody(
                                children: <Widget>[
                                  Text('Vrsta jahanja: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  Text(
                                    document['vrsta'],
                                    style: TextStyle(
                                        decoration: TextDecoration.underline),
                                  ),
                                  Text('Bilješka: ',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  Text(
                                    document['bilješka'],
                                    style: TextStyle(
                                        decoration: TextDecoration.underline),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ));
                  },
                  onLongPress: () async {
                    await showDialog(
                        context: context,
                        child: new AlertDialog(
                          content: new Text(
                            'Želite li obrisati sadržaj?',
                            style: TextStyle(color: Colors.brown),
                          ),
                          actions: <Widget>[
                            FlatButton(
                              child: Text(
                                'Odustani',
                                style: TextStyle(color: Colors.brown),
                              ),
                              onPressed: () {
                                Navigator.pop(context);
                              },
                            ),
                            FlatButton(
                                color: Colors.brown[400],
                                textColor: Colors.white,
                                child: Text(
                                  'Obriši',
                                ),
                                onPressed: () {
                                  FirebaseFirestore.instance
                                      .collection('posjetiRekreativaca')
                                      .doc(document.id)
                                      .delete();
                                  Navigator.pop(context);
                                })
                          ],
                        ));
                  },
                ),
              );
            }).toList(),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.brown[400],
        onPressed: () async {
          List<String> typesOfRiding = new List();
          await FirebaseFirestore.instance
              .collection('vrsteRekreativnogJahanja')
              .get()
              .then((querySnapshot) {
            querySnapshot.docs.forEach((element) {
              typesOfRiding.add(element['vrsta']);
            });
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) =>
                      AddNewVisit(typesOfRiding: typesOfRiding),
                ));
          });
        },
        tooltip: 'Dodaj',
        child: Icon(Icons.add),
      ),
    );
  }
}
