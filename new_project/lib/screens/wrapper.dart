import 'package:flutter/material.dart';
import 'package:new_project/screens/authenticate/authenticate.dart';
import 'package:new_project/screens/home/initialPage.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'home/home.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    print(user);

    //return either Home or Authenticate widget
    if (user == null) {
      return Initial();
    } else {
      return HomePage();
    }
  }
}
