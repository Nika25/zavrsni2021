import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

String timestampToString(Timestamp t) {
  var time = DateTime.fromMicrosecondsSinceEpoch(t.microsecondsSinceEpoch);
  return DateFormat.yMMMMd().format(time);
}

Future<DateTime> getDate(context) {
  return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light(),
          child: child,
        );
      });
}
