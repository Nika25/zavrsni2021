import 'package:flutter/material.dart';
import 'package:new_project/screens/school_of_horseback_riding/addNoteOfRidingStudent.dart';
import 'package:new_project/screens/school_of_horseback_riding/notesOfRidingStudent.dart';

class DynamicallyCheckbox extends StatefulWidget {
  final String name;
  final String surname;
  final String horse;
  final String ridingStudentId;
  final Map<String, bool> list;
  List<String> doneTasks;

  DynamicallyCheckbox(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      @required this.list,
      this.doneTasks});

  @override
  _DynamicallyCheckboxState createState() => _DynamicallyCheckboxState(
      list: this.list,
      name: this.name,
      surname: this.surname,
      ridingStudentId: this.ridingStudentId,
      horse: this.horse,
      doneTasks: this.doneTasks);
}

class _DynamicallyCheckboxState extends State<DynamicallyCheckbox> {
  final String name;
  final String surname;
  final String horse;
  final String ridingStudentId;
  Map<String, bool> list;
  List<String> doneTasks;

  _DynamicallyCheckboxState(
      {@required this.name,
      @required this.surname,
      @required this.horse,
      @required this.ridingStudentId,
      @required this.list,
      this.doneTasks});

  var holder_1 = [];

  List<String> getItems() {
    if (doneTasks == null) {
      doneTasks = new List();
    }
    list.forEach((key, value) {
      if (value == true) {
        holder_1.add(key);
        doneTasks.add(key);
      }
    });

    print(holder_1);

    holder_1.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: ListView(
            children: list.keys.map((String key) {
              return new CheckboxListTile(
                title: Text(key),
                value: list[key],
                activeColor: Colors.deepOrange,
                checkColor: Colors.white,
                onChanged: (bool value) {
                  setState(() {
                    list[key] = value;
                  });
                },
              );
            }).toList(),
          ),
        ),
        FlatButton(
          color: Colors.brown[200],
          textColor: Colors.white,
          child: Text('Spremi'),
          onPressed: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => NotesRidingStudent(
                    name: this.name,
                    surname: this.surname,
                    horse: this.horse,
                    ridingStudentId: this.ridingStudentId,
                    doneTasks: getItems())));
          },
        ),
      ],
    );
  }
}
