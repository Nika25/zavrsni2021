import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import 'package:new_project/screens/wrapper.dart';
import 'package:new_project/services/auth.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(HomeApp());
}

class HomeApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Projekt R',
        theme: ThemeData(
          primarySwatch: Colors.brown,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: ElevatedButton.styleFrom(
                onPrimary: Colors.white,
                primary: Colors.brown[300],
                minimumSize: Size(100, 40)),
          ),
          textTheme: TextTheme(
            bodyText2: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Colors.brown),
            bodyText1: TextStyle(
                fontSize: 20.0,
                fontStyle: FontStyle.italic,
                decoration: TextDecoration.underline,
                color: Colors.brown[400]),
          ),
        ),
        home: Wrapper(),
      ),
    );
  }
}
