import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/services/database.dart';

class HorseDatabaseService {
  final CollectionReference horseCollection =
      FirebaseFirestore.instance.collection('konji');

  Stream<QuerySnapshot> get horses {
    return horseCollection.snapshots();
  }
}
