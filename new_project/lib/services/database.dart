import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:new_project/models/user.dart';

class DatabaseService {
  final String uid;
  DatabaseService({this.uid});

  //collection reference
  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  Future updateUserData(String sugars, String name, int strength) async {
    return await userCollection
        .doc(uid)
        .set({'sugars': sugars, 'name': name, 'strength': strength});
  }

  //userdata from snapshot
  User _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return User(
        uid: uid,
        name: snapshot.data()['name'],
        surname: snapshot.data()['surname'],
        age: snapshot.data()['surname']);
  }

  Stream<QuerySnapshot> get users {
    return userCollection.get().asStream();
  }

  //get user doc stream
  Stream<DocumentSnapshot> get users2 {
    return userCollection.doc(uid).snapshots();
  }
}
